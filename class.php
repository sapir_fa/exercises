<?php
 class Htmlpage { 
    protected $title = "Sapir Fanani";
    protected $body = "Welcome to my page";
    public function view(){
        echo 
        "<html>
        <head>
          <title>$this->title</title>
        </head>
        <body>
            $this->body
        </body>
        </html>";
    }
    function __construct($title = "",$body = ""){
        if ($title != ""){
            $this->title = $title;
        }
        if ($body  != ""){
            $this->body = $body;
        }
    } 
 }

 class Colorspage extends Htmlpage {
    protected $color= 'green';
    public function __set($property,$value){
        if ($property == 'color'){
            $colors = array('black','gray','blue','pink');
            if (in_array($value, $colors)){
                $this->color = $value;
            }
            else{
                $this->body="This is an error message. Please change the color.";
            }
        }
    }
    public function view (){
      echo "<p style = 'color:$this->color'>$this->body</p>";
    }
 }

 class Sizesfontpage extends Colorspage {
    protected $size=20;
    public function __set($property,$value){
        if ($property == 'size'){
            $sizes = range(10,24);
            if (in_array($value, $sizes)){
                $this->size= $value;
            }
            else{
                $this->body="This is an error message. Please change the size of the font.";
            }
        }
        elseif ($property =='color'){
            parent::__set ($property,$value);
        }  
    }
    public function view (){
      echo "<p style = 'font-size:$this->size; color: $this->color'>$this->body</p>";
    }
 }
?>