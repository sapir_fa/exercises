<?php
    include "class.php";
?>
<?php
    $msg = new Htmlpage ();
    $msg->view();
    echo '<br>';
    $msg1 = new Colorspage();
    $msg1->color = 'pink';
    $msg1->view();
    echo '<br>';
    $msg2 = new Colorspage();
    $msg2->color = 'orange';
    $msg2->view();
    echo '<br>';
    $msg3 = new Sizesfontpage();
    $msg3->size = 10;
    $msg3->color = 'pink';
    $msg3->view();
    echo '<br>';
    $msg4 = new Sizesfontpage();
    $msg4->size = 14;
    $msg4->color = 'orange';
    $msg4->view();
    echo '<br>';
    $msg5 = new Sizesfontpage();
    $msg5->size = 12.5;
    $msg5->color = 'gray';
    $msg5->view();
?>